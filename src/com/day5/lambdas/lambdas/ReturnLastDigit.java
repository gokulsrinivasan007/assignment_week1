package com.day5.lambdas.lambdas;

 
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReturnLastDigit {
	public  List<Integer> rightDigit(List<Integer> nums) {
		
	Scanner scan = new Scanner(System.in);
		while (scan.hasNextInt()) {
			int i = scan.nextInt();
			nums.add(i);
		}scan.close();
	    nums.replaceAll(n -> n % 10);
	    return nums;   
	}
	
	public static void main(String args[]) {
		List<Integer> nums= new ArrayList<Integer>();
		ReturnLastDigit r1=new ReturnLastDigit();
		System.out.println(r1.rightDigit(nums));
	}
			
}