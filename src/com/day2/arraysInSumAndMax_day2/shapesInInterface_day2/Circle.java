package com.day2.arraysInSumAndMax_day2.shapesInInterface_day2;

public class Circle implements Shape {

	private int radius;

	Circle(int r) {
		radius = r;
	}

	public double area() {
		return Math.PI * radius * radius;
	}
}
