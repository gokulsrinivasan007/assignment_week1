package com.day2.arraysInSumAndMax_day2.shapesInInterface_day2;


public class Rectangle implements Shape {

	private double width;
	private double height;

	Rectangle(double w, double h) {
		width = w;
		height = h;
	}

	public double area() {
		return width * height;
	}

}
