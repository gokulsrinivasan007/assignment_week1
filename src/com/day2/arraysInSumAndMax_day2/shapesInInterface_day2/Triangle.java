package com.day2.arraysInSumAndMax_day2.shapesInInterface_day2;

public class Triangle implements Shape {

	private double height;
	private double base;

	Triangle(double h, double b) {
		height = h;
		base = b;
	}

	public double area() {
		return height * base / 2;

	}

}