package com.day2.arraysInSumAndMax_day2.shapesInInterface_day2;

public interface Shape {
	double area();

	public static void main(String s[]) {
		Shape obj = new Rectangle(50, 30);
		double objangleOfArea = obj.area();
		System.out.println("Area of objangle : " + objangleOfArea);

		obj = new Circle(33);
		double circleOfArea = obj.area();
		System.out.println("Area of circle : " + circleOfArea);

		obj   = new Triangle(20, 30);
		double triangleOfArea = obj.area();
		System.out.println("Area of triangle : " + triangleOfArea);
	}
}