package com.day2.arraysInSumAndMax_day2;

import java.util.Scanner;

public class TwoDimArray {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int[][] array = new int[2][2];
		int maxI = 0, maxJ = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				System.out.println("array[" + i + "," + j + "]=");
				array[i][j] = scanner.nextInt();
			}

		}
		scanner.close();

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				if (array[i][j] > array[maxI][maxJ]) {
					maxI = i;
					maxJ = j;
				}

			}
		}
		System.out.println("Max of array is : " + array[maxI][maxJ]);
		System.out.println("Position of max array is : [" + maxI + "," + maxJ + "]");
	}

}
