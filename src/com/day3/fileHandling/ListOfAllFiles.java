package com.day3.fileHandling;

import java.io.File;

public class ListOfAllFiles {
	public static void main(String a[]) {
		File file = new File("/Users/gokul/Documents/GOKUL");
		File[] listOfFiles = file.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				System.out.println("File " + listOfFiles[i].getName());
			} else if (listOfFiles[i].isDirectory()) {
				System.out.println("Directory " + listOfFiles[i].getName());
			}
		}
	}
}
