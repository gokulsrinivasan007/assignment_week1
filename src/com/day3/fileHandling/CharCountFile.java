package com.day3.fileHandling;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class CharCountFile {
	public static void main(String[] args) throws IOException {

		BufferedReader readFile = new BufferedReader(new FileReader("/Users/gokul/Documents/GOKUL/sample3.txt"));
		int char1;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a char: ");
		char search = scanner.next().charAt(0);
		int count = 0;
		while ((char1 = readFile.read()) != -1) {
			if (search == (char) char1) {
				count++;
			}
		}
		;
		readFile.close();
		scanner.close();
		System.out.println("TotalCharcters:" + count + " times");

	}

}
